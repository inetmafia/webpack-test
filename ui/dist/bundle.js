'use strict';

Object.defineProperty(exports, '__esModule', { value: true });

const button = () => {
  console.log('I am button');
};

const table = () => {
  console.log('I am table');
};

exports.button = button;
exports.table = table;
