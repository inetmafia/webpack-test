module.exports = {
  mode: 'none',
  entry: './src/index.js',
  output: {
    filename: '[name].bundle.js',
  },
  optimization: {
    sideEffects: true,
  },
}